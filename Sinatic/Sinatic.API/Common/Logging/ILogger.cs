namespace Sinatic.API.Common.Logging
{
    public interface ILogger
    {
        void Trace(string message);
        void Warn(string message);
        void Debug(string message);
        void Success(string message);
        void Error(string message);
    }
}