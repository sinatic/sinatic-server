namespace Sinatic.Enum.Common
{
    public enum LogLevel
    {
        Trace,
        Warn,
        Debug,
        Success,
        Error
    }
}