namespace Sinatic.Core.Communication.ConnectionManager
{
    public enum ConnectionState
    {
        Open = 0,
        Closed = 1,
    }
}