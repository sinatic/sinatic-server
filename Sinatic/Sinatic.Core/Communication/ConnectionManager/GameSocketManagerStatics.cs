namespace Sinatic.Core.Communication.ConnectionManager
{
    public static class GameSocketManagerStatics
    {
        public static readonly int BufferSize = 8192;
    }
}