namespace Sinatic.Core.Communication.RCON.Commands
{
    public interface IRconCommand
    {
        bool TryExecute(string[] parameters);
    }
}