namespace Sinatic.Core.Communication.Interfaces
{
    public interface IServerPacket
    {
        byte[] GetBytes();
    }
}