namespace Sinatic.Core.Communication.Packets.Outgoing.Messenger
{
    using HabboHotel.Users.Messenger;

    class InstantMessageErrorComposer : ServerPacket
    {
        public InstantMessageErrorComposer(MessengerMessageErrors Error, int Target)
            : base(ServerPacketHeader.InstantMessageErrorMessageComposer)
        {
            WriteInteger(MessengerMessageErrorsUtility.GetMessageErrorPacketNum(Error));
            WriteInteger(Target);
            WriteString("");
        }
    }
}