namespace Sinatic.Core.Communication.Packets.Outgoing.Rooms.Furni
{
    using System;

    class UpdateMagicTileComposer : ServerPacket
    {
        public UpdateMagicTileComposer(int ItemId, int Decimal)
            : base(ServerPacketHeader.UpdateMagicTileMessageComposer)
        {
            WriteInteger(Convert.ToInt32(ItemId));
            WriteInteger(Decimal);
        }
    }
}