namespace Sinatic.Core.Communication.Packets.Outgoing.Rooms.Settings
{
    using HabboHotel.Rooms;

    class GetRoomFilterListComposer : ServerPacket
    {
        public GetRoomFilterListComposer(Room Instance)
            : base(ServerPacketHeader.GetRoomFilterListMessageComposer)
        {
            WriteInteger(Instance.WordFilterList.Count);
            foreach (var Word in Instance.WordFilterList)
            {
                WriteString(Word);
            }
        }
    }
}