namespace Sinatic.Core.Communication.Packets.Outgoing.Rooms.Avatar
{
    using HabboHotel.Rooms;

    class DanceComposer : ServerPacket
    {
        public DanceComposer(RoomUser Avatar, int Dance)
            : base(ServerPacketHeader.DanceMessageComposer)
        {
            WriteInteger(Avatar.VirtualId);
            WriteInteger(Dance);
        }
    }
}