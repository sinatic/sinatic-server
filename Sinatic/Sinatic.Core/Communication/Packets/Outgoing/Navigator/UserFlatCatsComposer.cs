namespace Sinatic.Core.Communication.Packets.Outgoing.Navigator
{
    using System.Collections.Generic;
    using HabboHotel.Navigator;

    class UserFlatCatsComposer : ServerPacket
    {
        public UserFlatCatsComposer(ICollection<SearchResultList> categories, int rank)
            : base(ServerPacketHeader.UserFlatCatsMessageComposer)
        {
            WriteInteger(categories.Count);
            foreach (var category in categories)
            {
                WriteInteger(category.Id);
                WriteString(category.PublicName);
                WriteBoolean(category.RequiredRank <= rank);
                WriteBoolean(false);
                WriteString(string.Empty);
                WriteString(string.Empty);
                WriteBoolean(false);
            }
        }
    }
}