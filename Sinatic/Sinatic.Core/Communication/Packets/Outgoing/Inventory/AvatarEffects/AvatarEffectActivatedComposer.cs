namespace Sinatic.Core.Communication.Packets.Outgoing.Inventory.AvatarEffects
{
    using HabboHotel.Users.Effects;

    class AvatarEffectActivatedComposer : ServerPacket
    {
        public AvatarEffectActivatedComposer(AvatarEffect Effect)
            : base(ServerPacketHeader.AvatarEffectActivatedMessageComposer)
        {
            WriteInteger(Effect.SpriteId);
            WriteInteger((int) Effect.Duration);
            WriteBoolean(false); //Permanent
        }
    }
}