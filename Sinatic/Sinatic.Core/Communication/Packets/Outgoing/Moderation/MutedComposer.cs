namespace Sinatic.Core.Communication.Packets.Outgoing.Moderation
{
    using System;

    class MutedComposer : ServerPacket
    {
        public MutedComposer(double TimeMuted)
            : base(ServerPacketHeader.MutedMessageComposer)
        {
            WriteInteger(Convert.ToInt32(TimeMuted));
        }
    }
}