namespace Sinatic.Core.Communication.Packets.Incoming.Catalog
{
    using HabboHotel.GameClients;
    using Outgoing.Catalog;

    class GetGroupFurniConfigEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            session.SendPacket(new GroupFurniConfigComposer(PlusEnvironment.GetGame().GetGroupManager()
                .GetGroupsForUser(session.GetHabbo().Id)));
        }
    }
}