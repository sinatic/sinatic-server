namespace Sinatic.Core.Communication.Packets.Incoming.Catalog
{
    using HabboHotel.GameClients;
    using Outgoing.Catalog;

    class GetCatalogOfferEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            var offerId = packet.PopInt();
            if (!PlusEnvironment.GetGame().GetCatalog().ItemOffers.ContainsKey(offerId))
            {
                return;
            }

            var pageId = PlusEnvironment.GetGame().GetCatalog().ItemOffers[offerId];

            if (!PlusEnvironment.GetGame().GetCatalog().TryGetPage(pageId, out var page))
            {
                return;
            }

            if (!page.Enabled || !page.Visible || page.MinimumRank > session.GetHabbo().Rank ||
                (page.MinimumVIP > session.GetHabbo().VIPRank && session.GetHabbo().Rank == 1))
            {
                return;
            }

            if (!page.ItemOffers.ContainsKey(offerId))
            {
                return;
            }

            var item = page.ItemOffers[offerId];
            if (item != null)
            {
                session.SendPacket(new CatalogOfferComposer(item));
            }
        }
    }
}