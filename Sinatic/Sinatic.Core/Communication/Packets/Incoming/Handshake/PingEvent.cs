namespace Sinatic.Core.Communication.Packets.Incoming.Handshake
{
    using HabboHotel.GameClients;

    class PingEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            session.PingCount = 0;
        }
    }
}