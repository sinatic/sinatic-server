namespace Sinatic.Core.Communication.Packets.Incoming.Handshake
{
    using HabboHotel.GameClients;

    public class GetClientVersionEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            var build = packet.PopString();

            if (PlusEnvironment.SWFRevision != build)
            {
                PlusEnvironment.SWFRevision = build;
            }
        }
    }
}