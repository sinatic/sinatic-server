namespace Sinatic.Core.Communication.Packets.Incoming.Handshake
{
    using HabboHotel.GameClients;

    public class SsoTicketEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            if (session == null || session.GetHabbo() != null)
            {
                return;
            }


            var sso = packet.PopString();
            if (string.IsNullOrEmpty(sso) || sso.Length < 15)
            {
                return;
            }

            session.TryAuthenticate(sso);
        }
    }
}