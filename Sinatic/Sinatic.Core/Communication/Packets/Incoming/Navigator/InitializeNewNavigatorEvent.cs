namespace Sinatic.Core.Communication.Packets.Incoming.Navigator
{
    using HabboHotel.GameClients;
    using Outgoing.Navigator.New;

    class InitializeNewNavigatorEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            var topLevelItems = PlusEnvironment.GetGame().GetNavigator().GetTopLevelItems();

            session.SendPacket(new NavigatorMetaDataParserComposer(topLevelItems));
            session.SendPacket(new NavigatorLiftedRoomsComposer());
            session.SendPacket(new NavigatorCollapsedCategoriesComposer());
            session.SendPacket(new NavigatorPreferencesComposer());
        }
    }
}