namespace Sinatic.Core.Communication.Packets.Incoming.Talents
{
    using HabboHotel.GameClients;
    using Outgoing.Talents;

    class GetTalentTrackEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            var type = packet.PopString();

            var levels = PlusEnvironment.GetGame().GetTalentTrackManager().GetLevels();

            session.SendPacket(new TalentTrackComposer(levels, type));
        }
    }
}