namespace Sinatic.Core.Communication.Packets.Incoming.Help
{
    using HabboHotel.GameClients;
    using Outgoing.Help;

    class SendBullyReportEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            session.SendPacket(new SendBullyReportComposer());
        }
    }
}