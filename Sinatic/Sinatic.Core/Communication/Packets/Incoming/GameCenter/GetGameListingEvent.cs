namespace Sinatic.Core.Communication.Packets.Incoming.GameCenter
{
    using Outgoing.GameCenter;

    class GetGameListingEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            var Games = PlusEnvironment.GetGame().GetGameDataManager().GameData;

            Session.SendPacket(new GameListComposer(Games));
        }
    }
}