namespace Sinatic.Core.Communication.Packets.Incoming.Sound
{
    using HabboHotel.GameClients;
    using Outgoing.Sound;

    class GetSongInfoEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            session.SendPacket(new TraxSongInfoComposer());
        }
    }
}