namespace Sinatic.Core.Communication.Packets.Incoming.Inventory.Furni
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Utilities.Extentions;
    using HabboHotel.GameClients;
    using HabboHotel.Items;
    using Outgoing.Inventory.Furni;

    class RequestFurniInventoryEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            var items = session.GetHabbo().GetInventoryComponent().GetWallAndFloor;

            var page = 0;
            var enumerable1 = items as Item[] ?? items.ToArray();
            var pages = ((enumerable1.Count() - 1) / 700) + 1;

            if (!enumerable1.Any())
            {
                session.SendPacket(new FurniListComposer(enumerable1.ToList(), 1, 0));
            }
            else
            {
                foreach (var enumerable in enumerable1.Batch(700))
                {
                    var batch = (ICollection<Item>) enumerable;
                    session.SendPacket(new FurniListComposer(batch.ToList(), pages, page));

                    page++;
                }
            }
        }
    }
}