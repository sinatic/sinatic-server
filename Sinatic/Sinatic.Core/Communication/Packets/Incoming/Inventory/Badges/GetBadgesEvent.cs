namespace Sinatic.Core.Communication.Packets.Incoming.Inventory.Badges
{
    using HabboHotel.GameClients;
    using Outgoing.Inventory.Badges;

    class GetBadgesEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            session.SendPacket(new BadgesComposer(session));
        }
    }
}