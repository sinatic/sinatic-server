namespace Sinatic.Core.Communication.Packets.Incoming.Groups
{
    using HabboHotel.GameClients;
    using Outgoing.Groups;

    class GetGroupInfoEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            var groupId = packet.PopInt();
            var newWindow = packet.PopBoolean();

            if (!PlusEnvironment.GetGame().GetGroupManager().TryGetGroup(groupId, out var group))
            {
                return;
            }

            session.SendPacket(new GroupInfoComposer(group, session, newWindow));
        }
    }
}