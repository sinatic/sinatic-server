namespace Sinatic.Core.Communication.Packets.Incoming.LandingView
{
    using HabboHotel.GameClients;
    using Outgoing.LandingView;

    class GetPromoArticlesEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            var landingPromotions = PlusEnvironment.GetGame().GetLandingManager().GetPromotionItems();

            session.SendPacket(new PromoArticlesComposer(landingPromotions));
        }
    }
}