namespace Sinatic.Core.Communication.Packets.Incoming.Users
{
    using System;
    using System.Linq;
    using HabboHotel.GameClients;
    using Outgoing.Users;

    class GetRelationshipsEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            var habbo = PlusEnvironment.GetHabboById(packet.PopInt());
            if (habbo == null)
            {
                return;
            }

            var rand = new Random();
            habbo.Relationships = habbo.Relationships.OrderBy(x => rand.Next())
                .ToDictionary(item => item.Key, item => item.Value);

            session.SendPacket(new GetRelationshipsComposer(habbo));
        }
    }
}