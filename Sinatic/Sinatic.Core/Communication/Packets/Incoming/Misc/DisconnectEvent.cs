namespace Sinatic.Core.Communication.Packets.Incoming.Misc
{
    using HabboHotel.GameClients;

    class DisconnectEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            session.Disconnect();
        }
    }
}