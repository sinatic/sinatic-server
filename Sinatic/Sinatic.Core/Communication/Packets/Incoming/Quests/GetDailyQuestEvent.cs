namespace Sinatic.Core.Communication.Packets.Incoming.Quests
{
    using HabboHotel.GameClients;
    using Outgoing.LandingView;

    class GetDailyQuestEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            var usersOnline = PlusEnvironment.GetGame().GetClientManager().Count;

            session.SendPacket(new ConcurrentUsersGoalProgressComposer(usersOnline));
        }
    }
}