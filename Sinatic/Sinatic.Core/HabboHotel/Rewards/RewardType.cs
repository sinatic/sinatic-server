namespace Sinatic.Core.HabboHotel.Rewards
{
    public enum RewardType
    {
        Badge,
        Credits,
        Duckets,
        Diamonds,
        None
    }
}