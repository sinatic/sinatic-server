namespace Sinatic.Core.HabboHotel.Users.Messenger
{
    public enum MessengerMessageErrors
    {
        FriendMuted,
        yourMuted,
        FriendOffline,
        NotFriends,
        FriendBusy,
        OfflineFailed
    }
}