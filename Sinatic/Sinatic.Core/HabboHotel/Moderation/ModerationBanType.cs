namespace Sinatic.Core.HabboHotel.Moderation
{
    public enum ModerationBanType
    {
        IP,
        Machine,
        Username
    }
}