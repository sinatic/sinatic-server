namespace Sinatic.Core.HabboHotel.Rooms.AI
{
    public enum PetDatabaseUpdateState
    {
        Updated,
        NeedsUpdate,
        NeedsInsert
    }
}