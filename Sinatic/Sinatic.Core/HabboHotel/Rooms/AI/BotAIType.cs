namespace Sinatic.Core.HabboHotel.Rooms.AI
{
    public enum BotAIType
    {
        Pet,
        Generic,
        Bartender,
        CasinoBot
    }
}