namespace Sinatic.Core.HabboHotel.Rooms.Chat.Emotions
{
    enum ChatEmotions
    {
        Smile,
        Angry,
        Sad,
        Shocked,
        None
    }
}