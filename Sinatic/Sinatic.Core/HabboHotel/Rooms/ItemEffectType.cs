namespace Sinatic.Core.HabboHotel.Rooms
{
    public enum ItemEffectType
    {
        None,
        Swim,
        SwimLow,
        SwimHalloween,
        Iceskates,
        Normalskates
    }
}