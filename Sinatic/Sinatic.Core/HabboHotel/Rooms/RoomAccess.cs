namespace Sinatic.Core.HabboHotel.Rooms
{
    public enum RoomAccess
    {
        Open,
        Doorbell,
        Password,
        Invisible
    }
}