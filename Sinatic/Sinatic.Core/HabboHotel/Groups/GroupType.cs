namespace Sinatic.Core.HabboHotel.Groups
{
    public enum GroupType
    {
        Open = 0,
        Locked = 1,
        Private = 2
    }
}