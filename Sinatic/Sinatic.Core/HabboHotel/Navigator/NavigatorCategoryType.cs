namespace Sinatic.Core.HabboHotel.Navigator
{
    public enum NavigatorCategoryType
    {
        Category,
        Featured,
        Popular,
        Recommended,
        Query,
        MyRooms,
        MyFavourites,
        MyGroups,
        MyHistory,
        MyFriendsRooms,
        MyFrequentHistory,
        TopPromotions,
        PromotionCategory,
        MyRights
    }
}