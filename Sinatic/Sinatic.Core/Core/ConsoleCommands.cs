namespace Sinatic.Core.Core
{
    using System;
    using API.Common.Logging;
    using Common.Logging;
    using Communication.Packets.Outgoing.Moderation;

    public static class ConsoleCommands
    {
        private static readonly ILogger Logger = new ConsoleLogger();

        public static void InvokeCommand(string inputData)
        {
            if (string.IsNullOrEmpty(inputData))
            {
                return;
            }

            try
            {
                var parameters = inputData.Split(' ');

                switch (parameters[0].ToLower())
                {
                    #region stop

                    case "stop":
                    case "shutdown":
                    {
                        Logger.Warn(
                            "The server is saving users furniture, rooms, etc. WAIT FOR THE SERVER TO CLOSE, DO NOT EXIT THE PROCESS IN TASK MANAGER!!");
                        PlusEnvironment.PerformShutDown();
                        break;
                    }

                    #endregion

                    #region alert

                    case "alert":
                    {
                        var notice = inputData.Substring(6);

                        PlusEnvironment.GetGame().GetClientManager().SendPacket(
                            new BroadcastMessageAlertComposer(
                                PlusEnvironment.GetLanguageManager().TryGetValue("server.console.alert") + "\n\n" +
                                notice));

                        Logger.Trace("Alert successfully sent.");
                        break;
                    }

                    #endregion

                    default:
                    {
                        Logger.Error(parameters[0].ToLower() +
                                     " is an unknown or unsupported command. Type help for more information");
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error("Error in command [" + inputData + "]: " + e);
            }
        }
    }
}