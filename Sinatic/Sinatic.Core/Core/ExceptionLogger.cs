namespace Sinatic.Core.Core
{
    using System;
    using API.Common.Logging;
    using Common.Logging;

    public static class ExceptionLogger
    {
        private static readonly ILogger Logger = new ConsoleLogger();

        public static void LogQueryError(string query, Exception exception)
        {
            Logger.Error("Error in query:\r\n" + query + "\r\n" + exception + "\r\n\r\n");
        }

        public static void LogException(Exception exception)
        {
            Logger.Error("Exception:\r\n" + exception + "\r\n\r\n");
        }

        public static void LogCriticalException(Exception exception)
        {
            Logger.Error("Critical Exception:\r\n" + exception + "\r\n\r\n");
        }

        public static void LogThreadException(Exception exception)
        {
            Logger.Error("Thread Exception:\r\n" + exception + "\r\n\r\n");
        }

        public static void LogWiredException(Exception exception)
        {
            Logger.Error("Wired Exception:\r\n" + exception + "\r\n\r\n");
        }
    }
}