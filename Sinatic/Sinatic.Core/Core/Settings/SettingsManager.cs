namespace Sinatic.Core.Core.Settings
{
    using System.Collections.Generic;
    using System.Data;
    using API.Common.Logging;
    using Common.Logging;

    public class SettingsManager
    {
        private readonly Dictionary<string, string> _settings;

        private static ILogger Logger = new ConsoleLogger();

        public SettingsManager()
        {
            _settings = new Dictionary<string, string>();
        }

        public void Init()
        {
            if (_settings.Count > 0)
            {
                _settings.Clear();
            }

            using (var dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `server_settings`");
                var table = dbClient.GetTable();

                if (table != null)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        _settings.Add(row["key"].ToString().ToLower(), row["value"].ToString().ToLower());
                    }
                }
            }

            Logger.Trace("Loaded " + _settings.Count + " server settings.");
        }

        public string TryGetValue(string value)
        {
            return _settings.ContainsKey(value) ? _settings[value] : "0";
        }
    }
}