namespace Sinatic.Core.Core.Language
{
    using System.Collections.Generic;
    using System.Data;
    using API.Common.Logging;
    using Common.Logging;

    public class LanguageManager
    {
        private readonly Dictionary<string, string> _values;
        private static readonly ILogger Logger = new ConsoleLogger();

        public LanguageManager()
        {
            _values = new Dictionary<string, string>();
        }

        public void Init()
        {
            if (_values.Count > 0)
            {
                _values.Clear();
            }

            using (var dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `server_locale`");
                var Table = dbClient.GetTable();

                if (Table != null)
                {
                    foreach (DataRow Row in Table.Rows)
                    {
                        _values.Add(Row["key"].ToString(), Row["value"].ToString());
                    }
                }
            }

            Logger.Trace("Loaded " + _values.Count + " language locales.");
        }

        public string TryGetValue(string value)
        {
            return _values.ContainsKey(value) ? _values[value] : "No language locale found for [" + value + "]";
        }
    }
}