namespace Sinatic.Common.Logging
{
    using System;
    using System.IO;

    public class FileLogger
    {
        private readonly string _filePath;
        private readonly Type _className;

        protected FileLogger(Type className)
        {
            _filePath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) +
                        "/resources/logging/";
            _className = className;
        }

        protected void LogToFile(string file, string message)
        {
            var fullPath = Path.Combine(_filePath, file);

            Directory.CreateDirectory(_filePath);

            if (!File.Exists(fullPath))
            {
                File.Create(fullPath).Close();
            }

            using (var streamWriter = new StreamWriter(_filePath + file, true))
            {
                streamWriter.WriteLine(
                    $"Occurred at [{DateTime.Now:MM/dd HH:mm:ss}] in [{_className.FullName}]: {message}");
            }
        }
    }
}