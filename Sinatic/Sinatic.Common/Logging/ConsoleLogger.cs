namespace Sinatic.Common.Logging
{
    using System;
    using System.Collections.Generic;
    using API.Common.Logging;
    using Enum.Common;

    public class ConsoleLogger : FileLogger, ILogger
    {
        public ConsoleLogger() : base(null)
        {
        }

        private readonly Dictionary<LogLevel, string> _fileForLogLevel = new Dictionary<LogLevel, string>
        {
            {LogLevel.Trace, "trace.log"},
            {LogLevel.Warn, "warn.log"},
            {LogLevel.Debug, "debug.log"},
            {LogLevel.Success, "success.log"},
            {LogLevel.Error, "error.log"}
        };

        private readonly Dictionary<LogLevel, ConsoleColor> _colorForLogLevel = new Dictionary<LogLevel, ConsoleColor>
        {
            {LogLevel.Trace, ConsoleColor.White},
            {LogLevel.Warn, ConsoleColor.Yellow},
            {LogLevel.Debug, ConsoleColor.Cyan},
            {LogLevel.Success, ConsoleColor.Green},
            {LogLevel.Error, ConsoleColor.Red}
        };

        public void Trace(string message)
        {
            Log(message, LogLevel.Trace);
        }

        public void Warn(string message)
        {
            Log(message, LogLevel.Warn, true);
        }

        public void Debug(string message)
        {
            Log(message, LogLevel.Debug, true);
        }

        public void Success(string message)
        {
            Log(message, LogLevel.Success, true);
        }

        public void Error(string message)
        {
            Log(message, LogLevel.Error, true);
        }

        private void Log(string message, LogLevel logLevel, bool logToFile = false)
        {
            var oldColor = Console.ForegroundColor;

            Console.ForegroundColor = _colorForLogLevel[logLevel];
            Console.WriteLine($"  [{DateTime.Now:MM/dd HH:mm:ss}] " + message);
            Console.ForegroundColor = oldColor;

            if (logToFile)
            {
                LogToFile(_fileForLogLevel[logLevel], message);
            }
        }
    }
}